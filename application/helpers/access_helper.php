<?php

function logged_in() {

    $ci = get_instance();

    if (!$ci->session->userdata('username')) {
        redirect('login');
    }
}

function access_developer() {
    $ci = get_instance();
    if ($ci->session->userdata('level') !== 'DEVELOPER') {
        redirect('developer/blank');
    }
}

function access_admin() {
    $ci = get_instance();
    if ($ci->session->userdata('level') !== 'ADMINISTRATOR') {
        redirect('admin/blank');
    }
}

function access_operator() {
    $ci = get_instance();
    if ($ci->session->userdata('level') !== 'OPERATOR') {
        redirect('operator/blank');
    }
}

function access_profiling() {
    $ci = get_instance();
    if ($ci->session->userdata('level') !== 'PROFILING') {
        redirect('profiling/blank');
    }
}

function access_store() {
    $ci = get_instance();
    if ($ci->session->userdata('level') !== 'STORE') {
        redirect('store/blank');
    }
}