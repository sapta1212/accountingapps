<?php
	$role = $this->session->userdata('role');
?>
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">New Company</h3>
                </div>
                <?= $this->session->flashdata('message'); ?>
			<?php echo form_open_multipart('/company/saveCompany');?>
				<div class="form-horizontal" >
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-7">
                                <input type="text" name="name" class="form-control" id="name" autocomplete="off" placeholder="Company Name" value="<?= set_value('name') ?>">
                                <?= form_error('name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone_number" class="col-sm-4 control-label">Phone Number</label>
                            <div class="col-sm-7">
                                <input type="text" name="phone_number" class="form-control" autocomplete="off" id="phone_number" onkeypress="return hanyaAngka(event)" placeholder="Phone Number" value="<?= set_value('phone_number') ?>">
                                <?= form_error('phone_number', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-7">
                                <textarea autocomplete="off" class="form-control" name="address" id="address" placeholder="Address"><?= set_value('address'); ?></textarea>
                                <?= form_error('address', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic_01" class="col-sm-4 control-label">PIC 1</label>
                            <div class="col-sm-7">
                                <select name="pic_01" id="pic_01" class="form-control">
                                    <option hidden selected disabled="disabled" value="">--Choose PIC 2--</option>
                                        <?php 
                                            foreach($pic_name as $row => $value){
                                                foreach($value as $row => $nilai){
                                        ?>
                                        <option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        <?php } }?>
                                </select>
                                <?= form_error('pic_01', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic_02" class="col-sm-4 control-label">PIC 2</label>
                            <div class="col-sm-7">
                                <select name="pic_02" id="pic_02" class="form-control">
                                    <option hidden selected disabled="disabled" value="">--Choose PIC 2--</option>
                                        <?php 
                                            foreach($pic_name as $row => $value){
                                                foreach($value as $row => $nilai){
                                        ?>
                                        <option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        <?php } }?>
                                </select>
                                <?= form_error('pic_02', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic_03" class="col-sm-4 control-label">PIC 3</label>
                            <div class="col-sm-7">
                                <select name="pic_03" id="pic_03" class="form-control">
                                    <option hidden selected disabled="disabled" value="">--Choose PIC 3--</option>
                                        <?php 
                                            foreach($pic_name as $row => $value){
                                                foreach($value as $row => $nilai){
                                        ?>
                                        <option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        <?php } }?>
                                </select>
                                <?= form_error('pic_03', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
						<div class="form-group">
                            <label for="img" class="col-sm-4 control-label">Add Company</label>

                            <div class="col-sm-7">
                                <input type="file" name="img" class="form-control" id="img" accept=".png" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4"></div>
                            <div class="col-md-5">
                                <a href="<?php echo base_url('/company/dataCompany'); ?>" class="btn btn-default">Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    
                    <!-- /.box-footer -->
                    </div>
				<?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_lahir').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_lahir").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_lahir").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
        function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>