<?php
	$role = $this->session->userdata('role');
?>
<section class="content">
		<div class="box box-danger">
            <div class="success-sapta" data-flashdata="<?= $this->session->flashdata('success') ?>"></div>
            <div class="failed-sapta" data-flashdata="<?= $this->session->flashdata('failed') ?>"></div>
			<div class="box-header">
				<h3 class="box-title">DATA PENGGUNA</h3>
			</div>
			<div class="box-body">
				<div>
					<a class="btn btn-primary" href="<?= base_url($role.'/user/add')?>">Pengguna Baru</a>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<div class="box box-default">
		<div class="box-body table-responsive">
			<table id="example2" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>No.</th>
						<th>Username</th>
						<th>Level</th>
						<th>Tlg Pembuatan</th>
						<th colspan="2">Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php
						if(!$userdata){
							echo '<td>No data</td>';
						}else{
							$no = 1;
							$page = $this->uri->segment(4);
							$next = intval($page)+1;
							foreach((array)$userdata as $row){
							
						?>
							<td>
								<?php 
									if($page == null){
										echo $no++;
									}else{
										echo $next++;
									}
								?>
							</td>
							<td><?= $row->username; ?></td>
							<td><?= $row->role; ?></td>
							<td><?php echo date("d F Y", strtotime($row->createdate)) ?></td>
							<td class="text-center"
								<?php 
									if($row->status == 'true'){
										echo 'style="background-color:green; color:white;"';
									}else{
										echo 'style="background-color:red; color:white"';} 
								?>><b>
									<?php 
										if($row->status == 'true'){
											echo 'Aktif';
										}else{
											echo 'Tidak Aktif';}
									?>
							</b></td>
							<td>
                                	<a href="#" id="edit" class="data" data-toggle="modal" data-target="#modalEdit" data-username="<?= $row->username; ?>" data-status="<?php if($row->status == 'true'){echo 'Aktif'; }else{echo 'Tidak Aktif';} ?>" title="Ubah"><i class="fa fa-pencil"> Ubah</i></a></td>

					</tr>
					<?php }} ?>
				</tbody>
			</table>
	       <?php echo $halaman; ?>
		</div>
		</div>
</section>
<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalEdit" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cancel</span>
                                </button>
                                    <h4 class="modal-title" id="labelModalKu">Rubah Status</h4>
                        </div>
                            <div class="modal-body" id="body-replace">
                                <form id="form" enctype="multipart/form-data" action="<?php echo base_url($role.'/user/updateStatus'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Username</label>
                                            <input type="text" name="username" readonly="readonly" class="form-control" id="username" autocomplete="off" required="required">
									</div>
									<div class="form-group">
                            			<label for="status">Status</label>
                                			<select name="status" id="status" class="form-control">
                                    			<option value="Aktif">Aktif</option>
                                    			<option value="Tidak Aktif">Tidak Aktif</option>
                                			</select>
									</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <button type="submit" name="button" class="btn btn-primary">Ubah</a></button>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
	</div>


<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"> 
				jQuery(function($) {
                            var dateToday = new Date();
                            $('#tgl_bentuk').datepicker({
                                dateFormat: 'yyyy-mm-dd',
                                maxDate: dateToday,
                                todayHighlight: true,
                                autoclose: true,
                            });

                            $("#tgl_bentuk").on('changeDate', function(selected) {
                                var startDate = new Date(selected.date.valueOf());
                                $("#datepickerEnd").datepicker('setStartDate', startDate);
                                if ($("#tgl_bentuk").val() > $("#datepickerEnd").val()) {
                                    ;
                                }
                            });
                        })
</script>
<script type="text/javascript">
    $(document).on("click", "#edit", function() {
        var username = $(this).data("username");
        var status = $(this).data("status");

        $("#body-replace #username").val(username);
        $("#body-replace #status").val(status);
    })

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
