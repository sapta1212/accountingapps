<?php
	$role = $this->session->userdata('role');
?>
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-7">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Baru</h3>
                    </div>
                        <?= $this->session->flashdata('message'); ?>
                            <form class="form-horizontal" method="post" action="<?php echo base_url($role.'/user/registration'); ?>">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="username" class="col-sm-4 control-label">Username</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="username" class="form-control" id="username" placeholder="Username" value="<?= set_value('username') ?>">
                                                <?= form_error('username', '<span class="text-danger">', '</span>'); ?>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword1" class="col-sm-4 control-label">Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="password1" id="inputPassword1" placeholder="Password">
                                                <?= form_error('password1', '<span class="text-danger">', '</span>'); ?>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-sm-4 control-label">Ulangi Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="password2" id="inputPassword2" placeholder="Ulangi Password">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="level" class="col-sm-4 control-label">Akses User</label>
                                            <div class="col-sm-7">
                                                <select name="level" id="level" class="form-control">
                                                    <option hidden selected disabled="disabled" value="">--Pilih Akses User--</option>
                                                        <?php 
                                                            foreach ($role_access as $row => $value){
                                                                foreach($value as $row => $nilai){
                                                        ?>  
                                                    <option value="<?= $nilai; ?>"><?= $nilai ?></option>
                                                        <?php } }?>
                                                </select>
                                                <?= form_error('level', '<span class="text-danger">', '</span>'); ?>
                                            </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <a href="<?php echo base_url($role.'/user'); ?>" class="btn btn-default pull-left">Kembali</a>
                                        <button type="submit" class="btn btn-info pull-right">Simpan</button>
                                 </div>
                            </form>
                        </div>
            </div>
    </div>
</section>
