<?php
	$role = $this->session->userdata('role');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" />
<style>
.umpet{
	display: none;
}
</style>

<script type="text/javascript">

  function hideChoose() {

    // get value kategoru to show input
    getSelectValue = document.getElementById('searchby').value;
    if (getSelectValue != "") {
      document.getElementById('cari').style.display = "inline";
      document.getElementById("sea").style.display = "none";
    } else {
      document.getElementById('cari').style.display = "none";
    }


    if (getSelectValue == "new_pic") {
      document.getElementById('respon').style.display = "none";
      document.getElementById('cari').style.display = "none";
      document.getElementById('btn_new_pic').style.display = "inline";

    } else if(getSelectValue == "search_name_pic"){
      document.getElementById('respon').style.display = "inline";
      document.getElementById('cari').required = "true";
      document.getElementById('btn_new_pic').style.display = "none";

	}else {
      document.getElementById('respon').style.display = "none";
      document.getElementById('cari').style.display = "none";
      document.getElementById('btn_new_pic').style.display = "none";
    }


    for (var i = 0; i != hide.length; i++) {
      if (checkbox.checked) {
        hide[i].style.display = "inline";
        // hide[i].required="true";
      } else {
        hide[i].style.display = "none";
      }

    }
  }
</script>


<section class="content">
	<div class="box box-danger">
        <div class="success-sapta" data-flashdata="<?= $this->session->flashdata('success') ?>"></div>
        	<div class="failed-sapta" data-flashdata="<?= $this->session->flashdata('failed') ?>"></div>
				<div class="box-header">
					<h3 class="box-title">PIC DATA</h3>
				</div>
			<div class="box-body">
				<div>
                    <div class="col-sm-12">
       				<?php echo form_open('/pic/searchPic'); ?>
    				<div>
          				<select class="form-group btn btn-secondary" name="searchby" id="searchby" onchange="hideChoose()" style="margin-top:10px; height:32px;">
            				<option value="" disabled selected hidden id="sea">PIC Action: </option>
            				<option value="new_pic">New PIC</option>
            				<option value="search_name_pic">Search PIC Name</option>
					  	</select>
				  		<input type="text" name="keyword" value="" id="respon" autocomplete="off" class="umpet" required>
          				<input class="btn btn-small btn-primary umpet" name="search" type="submit" value="Find" id="cari">
					    <a class="btn btn-primary umpet" id="btn_new_pic" href="<?= base_url('/pic/addPic')?>">New PIC</a>
        			</div>
				<?php echo form_close(); ?>
				</div>
				</div>
			</div>
		</div>
		<div class="box box-default">
			<div class="box-body table-responsive">
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Address</th>
							<th colspan="2">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php
							if(!$data_pic){
								echo '<td>No data</td>';
							}else{
								$no = 1;
								$page = $this->uri->segment(4);
								$next = intval($page)+1;
								foreach($data_pic as $row){
							
							?>
								<td class="text-center">
									<?php 
										if($page == null){
											echo $no++;
										}else{
											echo $next++;
										}
									?>
								</td>
								<td><?= $row['name'] ?></td>
								<td><?= $row['email'] ?></td>
								<td><?= $row['phone'] ?></td>
								<td><?= $row['address'] ?></td>
								<td>
                                    <a href="#" id="edit" class="data" data-toggle="modal" data-target="#modalEdit" data-id_pic_edit="<?= $row['id_pic']; ?>" , 
                                    data-name_edit="<?= $row['name']; ?>" , data-email_edit="<?= $row['email']; ?>" , data-phone_edit="<?= $row['phone']; ?>" ,  
                                    data-address_edit="<?= $row['address']; ?>"  title="Ubah"><i class="fa fa-pencil"> Update</i></a>
                            	</td>
								<td>
                                	<a href="#" id="hapus" class="data" data-toggle="modal" data-target="#modalDeleteKegiatan" data-id_pic_delete="<?= $row['id_pic']; ?>" ,
									data-name_delete="<?= $row['name']; ?>"  title="Hapus"><i class="fa fa-trash"> Delete</i></a>
                            	</td>

						</tr>
							<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>
	<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalEdit" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cancel</span>
                                </button>
                                    <h4 class="modal-title" id="labelModalKu">Update PIC</h4>
                        </div>
                            <div class="modal-body" id="body-replace">
                                <form id="form" enctype="multipart/form-data" action="<?php echo base_url('pic/editPic'); ?>" method="post">
                                        <input type="text" name="id_pic_edit"  id="id_pic_edit" hidden>

                                    <div class="form-group">
                                        <label>PIC Name</label>
                                            <input type="text" name="name_edit" class="form-control" id="name_edit" autocomplete="off" required="required">
									</div>
                                    <div class="form-group">
                                        <label>Email</label>
                                            <input type="text" name="email_edit" class="form-control" id="email_edit" data-date-format='yyyy-mm-dd' autocomplete="off" required="required">
									</div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                            <input type="text" name="phone_edit" class="form-control" id="phone_edit" autocomplete="off" onkeypress = "return hanyaAngka(event)" required="required">
									</div>
                                    <div class="form-group">
                                        <label>Address</label>
                                            <textarea type="text" name="address_edit" class="form-control" id="address_edit" autocomplete="off" required="required"></textarea>
									</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <button type="submit" name="button" class="btn btn-primary">Ubah</a></button>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
	</div>
</section>
<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalDeleteKegiatan" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cancel</span>
                                </button>
                                    <h4 class="modal-title" id="labelModalKu">Confirm Delete</h4>
                        </div>
                            <div class="modal-body" id="body-delete">
                                <form id="form" enctype="multipart/form-data" action="<?php echo base_url('pic/deletePic'); ?>" method="post">
                                        	<input type="text" name="id_pic_delete"  id="id_pic_delete" hidden>

                                    <div class="form-group">
                                        <label>PIC Name</label>
                                            <input type="text" name="name_delete" readonly="readonly" class="form-control" id="name_delete" autocomplete="off" style="border-width:0px; font-size:20px; background-color: yellow" required="required">
									</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <button type="submit" name="button" class="btn btn-primary">Hapus</a></button>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
	</div>
<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
<script src="https://adminlte.io/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript"> 
				jQuery(function($) {
                            var dateToday = new Date();
                            $('#tgl_bentuk').datepicker({
                                dateFormat: 'yyyy-mm-dd',
                                maxDate: dateToday,
                                todayHighlight: true,
                                autoclose: true,
                            });

                            $("#tgl_bentuk").on('changeDate', function(selected) {
                                var startDate = new Date(selected.date.valueOf());
                                $("#datepickerEnd").datepicker('setStartDate', startDate);
                                if ($("#tgl_bentuk").val() > $("#datepickerEnd").val()) {
                                    ;
                                }
                            });
                        })

        function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
<script type="text/javascript"> 
				jQuery(function($) {
                            var dateToday = new Date();
                            $('#tgl_kegiatan').datepicker({
                                dateFormat: 'yyyy-mm-dd',
                                maxDate: dateToday,
                                todayHighlight: true,
                                autoclose: true,
                            });

                            $("#tgl_kegiatan").on('changeDate', function(selected) {
                                var startDate = new Date(selected.date.valueOf());
                                $("#datepickerEnd").datepicker('setStartDate', startDate);
                                if ($("#tgl_kegiatan").val() > $("#datepickerEnd").val()) {
                                    ;
                                }
                            });
                        })
</script>
<script type="text/javascript">
    $(document).on("click", "#edit", function() {
        var id_pic_edit = $(this).data("id_pic_edit");
        var name_edit = $(this).data("name_edit");
        var email_edit = $(this).data("email_edit");
        var phone_edit = $(this).data("phone_edit");
        var address_edit = $(this).data("address_edit");

        $("#body-replace #id_pic_edit").val(id_pic_edit);
        $("#body-replace #name_edit").val(name_edit);
        $("#body-replace #email_edit").val(email_edit);
        $("#body-replace #phone_edit").val(phone_edit);
        $("#body-replace #address_edit").val(address_edit);
    })

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
<script type="text/javascript">
    $(document).on("click", "#hapus", function() {
        var id_pic_delete = $(this).data("id_pic_delete");
        var name_delete = $(this).data("name_delete");

        $("#body-delete #id_pic_delete").val(id_pic_delete);
        $("#body-delete #name_delete").val(name_delete);
    })

</script>

<script type="text/javascript"> 
    $('.timepicker').timepicker({
      showInputs: false
    })
</script>