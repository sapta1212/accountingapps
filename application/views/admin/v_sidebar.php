<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo base_url('company/dataCompany'); ?>">
                    <i class="fa  fa-building"></i> <span>Company</span>
                </a>
            </li>
            
            <li>
                <a href="<?php echo base_url('pic/dataPic'); ?>">
                    <i class="fa fa-users"></i> <span>PICs</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>