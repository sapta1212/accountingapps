<?php
	$role = $this->session->userdata('role');
?>
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">New PIC</h3>
                </div>
                <?= $this->session->flashdata('message'); ?>
			<?php echo form_open_multipart('/pic/savePic');?>
				<div class="form-horizontal" >
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">PIC Name</label>
                            <div class="col-sm-7">
                                <input type="text" name="name" class="form-control" id="name" autocomplete="off" placeholder="Pic Name" value="<?= set_value('name') ?>">
                                <?= form_error('name', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-7">
                                <input type="text" name="email" class="form-control" id="email" autocomplete="off" placeholder="Email" value="<?= set_value('email') ?>">
                                <?= form_error('email', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Phone</label>
                            <div class="col-sm-7">
                                <input type="text" name="phone" class="form-control" autocomplete="off" id="phone" onkeypress="return hanyaAngka(event)" placeholder="Phone" value="<?= set_value('phone') ?>">
                                <?= form_error('phone', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-7">
                                <textarea autocomplete="off" class="form-control" name="address" id="address" placeholder="Address"><?= set_value('address'); ?></textarea>
                                <?= form_error('address', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4"></div>
                            <div class="col-md-5">
                                <a href="<?php echo base_url('/pic/dataPic'); ?>" class="btn btn-default">Back</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    
                    <!-- /.box-footer -->
                    </div>
				<?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_lahir').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_lahir").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_lahir").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
        function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_bergabung').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_bergabung").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_bergabung").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_pernikahan').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_pernikahan").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_pernikahan").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_babtis').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_babtis").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_babtis").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_sidi').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_sidi").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_sidi").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_keluar').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_keluar").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_keluar").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>