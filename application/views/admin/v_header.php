<?php
	$role = $this->session->userdata('role');
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('company/dataCompany'); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b></span>
        <!-- logo for regular state andmobile devices -->
        <span class="logo-lg"><b></b>Accounting Apps</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="<?php echo base_url('home'); ?>" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- Notifications: style can be found in dropdown.less -->

                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                    <a href="<?php echo base_url(); ?>assets/#" class="dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-calendar "></i>
											<span><?php echo date('Y-m-d'); ?></span>
											<i class="fa fa-clock-o"></i>
											<span id="jam"></span>
                    </a>
                    <ul class="dropdown-menu">

                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="<?php echo base_url(); ?>assets/#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">Sapta Nugraha</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<script type="text/javascript">
			window.onload = function() { jam(); }

				function jam() {
				var e = document.getElementById('jam'),
				d = new Date(), h, m, s;
				h = d.getHours();
				m = set(d.getMinutes());
				s = set(d.getSeconds());

				e.innerHTML = h +':'+ m +':'+ s;

				setTimeout('jam()', 1000);
				}

				function set(e) {
				e = e < 10 ? '0'+ e : e;
				return e;
				}
		</script>
