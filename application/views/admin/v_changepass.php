<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <!-- right column -->
        <div class="col-md-7">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Password</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->session->flashdata('message'); ?>
                <form class="form-horizontal" onSubmit="return validasi()" method="post" action="<?php echo base_url('admin/user/edit'); ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="username" class="col-sm-4 control-label">Username</label>

                            <div class="col-sm-7">
                                <input type="text" disabled name="username" class="form-control" id="username" placeholder="Username" value="<?= $this->session->userdata('username') ?>">
                                <?= form_error('username', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1" class="col-sm-4 control-label">New Password</label>

                            <div class="col-sm-7">
                                <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                                title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                class="form-control" name="password1" id="inputPassword1" placeholder="New Password">
                                <?= form_error('password1', '<span class="text-danger">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword2" class="col-sm-4 control-label">Confirm New Password</label>

                            <div class="col-sm-7">
                                <input type="password" class="form-control" name="password2" id="inputPassword2" placeholder="Confirm New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="oldpass" class="col-sm-4 control-label">Current Password</label>

                            <div class="col-sm-7">
                                <input type="password" class="form-control" name="oldpass" id="oldpass" placeholder="Current Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-4"></label>

                            <div class="col-sm-7">
								<a href="<?php echo base_url('admin/home');?>" class="btn btn-default">Cancel</a>
								<button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </div>
                    <!-- /.box-footer -->

                    </div>
                    <!-- /.box-body -->
                    
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>

<script type="text/javascript">
  function validasi (){
    var pass = document.getElementById('inputPassword1').value;
    var pass2 = document.getElementById('inputPassword2').value;
    var oldpasss = document.getElementById('oldpass').value;

    if (pass == oldpasss) {
      alert("New Password Can't be Same as Old!");
      return false;
    }else if (pass != pass2){
      alert("New Confirm Password Not Same!");
      return false;
    }else if(pass == pass2 == oldpasss){
      alert("New Password Can't be Same as Old!");
      return false;
    }else {
      return true;
    }

  }
</script>
