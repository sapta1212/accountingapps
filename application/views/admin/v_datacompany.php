<?php
	$role = $this->session->userdata('role');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" />
<style>
.umpet{
	display: none;
}
</style>

<script type="text/javascript">
  
  function hideChoose() {

    // get value kategoru to show input
    getSelectValue = document.getElementById('searchby').value;
    if (getSelectValue != "") {
      document.getElementById('cari').style.display = "inline";
      document.getElementById("sea").style.display = "none";
    } else {
      document.getElementById('cari').style.display = "none";
    }


    if (getSelectValue == "com_name") {
      document.getElementById('respon').style.display = "inline";
      document.getElementById('respon').required = "true";
      document.getElementById('new_company').style.display = "none";

    } else if(getSelectValue == "new_com"){
      document.getElementById('respon').style.display = "none";
      document.getElementById('cari').style.display = "none";
      document.getElementById('new_company').style.display = "inline";

	}else {
      document.getElementById('respon').style.display = "none";
      document.getElementById('cari').style.display = "none";
      document.getElementById('new_company').style.display = "none";
    }


    for (var i = 0; i != hide.length; i++) {
      if (checkbox.checked) {
        hide[i].style.display = "inline";
        // hide[i].required="true";
      } else {
        hide[i].style.display = "none";
      }

    }
  }
</script>

<section class="content">
		<div class="box box-danger">
            <div class="success-sapta" data-flashdata="<?= $this->session->flashdata('success') ?>"></div>
            <div class="failed-sapta" data-flashdata="<?= $this->session->flashdata('failed') ?>"></div>
            <div class="info-sapta" data-flashdata="<?= $this->session->flashdata('info') ?>"></div>
			<div class="box-header">
				<h3 class="box-title">COMPANY DATA</h3>
			</div>
			
			
			<div class="box-body">
				<div>
					<div class="col-sm-12">
       				<?php echo form_open($role.'/jemaat/searchJemaat'); ?>
    				<div>
          				<select class="form-group" name="searchby" id="searchby" onchange="hideChoose()" style="margin-top:10px; height:32px;">
            				<option value="" disabled selected hidden id="sea">Company Action: </option>
            				<option value="new_com">New Company</option>
            				<option value="com_name"> Search Company Name</option>
					  	</select>
				  		<input type="text" name="keyword" value="" id="respon" autocomplete="off" class="umpet" required>
          				<input class="btn btn-small btn-primary umpet" name="search" type="submit" value="Find" id="cari">
						<a class="btn umpet" id="new_company" href="<?= base_url('/company/addCompany')?>" style="background-color:#3c8dbc; color:white;">New Company</a>
        			</div>
				<?php echo form_close(); ?>
				</div>
				</div>
			</div>
			
		</div>
		<?php 
			if(!$data_company){
				echo 'Data No Available';
			}else{
				foreach($data_company as $row){
					
		?>

		<div class="col-md-4">
        	<div class="box box-widget widget-user">
                <div class="widget-user-header" style="background-color:#3c8dbc; color:white;">
              		<h3 class="widget-user-username"><?php echo $row['name']; ?></h3>
              		<h5 class="widget-user-desc"><?php echo $row['phone_number']; ?></h5>
              		<h5 class="widget-user-desc"><?php echo $row['address']; ?></h5>
            	</div>
            		<div class="widget-user-image">
              			<img class="img-circle" src="<?php $img = (FCPATH.'assets/upload/company/'.str_replace(" ","_",$row['name']).'.png');if(file_exists($img)){ echo base_url('assets/upload/company/'.str_replace(" ","_",$row['name']).'.png');  }else{ echo base_url('assets/upload/company/image_no.png'); } ?>" alt="User Avatar">
            		</div>
            			<div class="box-footer">
              				<div class="row">
                				<div class="col-sm-4 border-right">
                  					<div class="description-block">
									  <a href="#" id="edit_jemaat" class="data" title="Ubah Data" data-toggle="modal" data-target="#modalEditJemaat"  data-id_company_edit="<?= $row['id_company'];?>"
									  , data-name_edit="<?= $row['name'];?>", data-company_photo="<?php  $img = (FCPATH.'assets/upload/company/'.str_replace(" ","_",$row['name']).'.png');if(file_exists($img)){ echo base_url('assets/upload/company/'.str_replace(" ","_",$row['name']).'.png');  }else{ echo base_url('assets/upload/company/image_no.png'); } ?>"
									  data-phone_number_edit="<?= $row['phone_number'];?>" , data-address_edit="<?= $row['address'];?>" ,
									  data-pic1_edit="<?= $row['pic1'];?>" , data-pic2_edit="<?= $row['pic2'];?>" , data-pic3_edit="<?= $row['pic3'];?>"><i class="fa fa-pencil"> Ubah</i></a>
                  						</div>
                					</div>
                				<div class="col-sm-4 border-right">
                  					<div class="description-block">
									  <a href="#" id="info_jemaat" class="data" title="Data Lengkap" data-toggle="modal" data-target="#modalInfoJemaat" data-id_company_info="<?= $row['id_company'];?>"
                                      , data-name_info="<?= $row['name'];?>", data-phone_number_info="<?= $row['phone_number'];?>" , data-pic1_info="<?= $row['pic1']; ?>" , data-pic2_info="<?= $row['pic2']; ?>" , data-pic3_info="<?= $row['pic3']; ?>" 
                                       value="<?=isset($_POST[$row['name']]) ? $_POST[$row['name']] : ''?>" ><i class="fa fa-info"> Detail</i></a>
                  					</div>
                				</div>
                				<div class="col-sm-4">
                  					<div class="description-block">
									  <a href="#" id="delete_jemaat" class="data" title="Hapus Data" data-toggle="modal" data-target="#modalDeleteJemaat" data-id_company_delete="<?= $row['id_company'];?>"
									  , data-name_delete="<?= $row['name'];?>"><i class="fa fa-trash"> Delete</i></a>
                  					</div>
                				</div>
              				</div>
            		</div>
          		</div>
        	</div>                        
         <?php } } ?>  
</section>

	<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalInfoJemaat" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Cancel</span>
                            </button>
                                <h4 class="modal-title">Company Detail</h4>
                        </div>
                            <div class="modal-body" id="body-info">
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
										<input type="text" name="id_jemaat"  id="id_jemaat"  hidden> 
                                    <div class="form-group">
                                        <label>Nama Jemaat</label>
                                            <input type="text" name="nama_jemaat" class="form-control" id="nama_jemaat" readonly >
									</div>
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                            <input type="text" name="tgl_lahir" class="form-control" id="tgl_lahir" readonly >
						            </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                            <input type="text" name="jenis_kelamin" class="form-control" id="jenis_kelamin" readonly >
						            </div>
                                    <div class="form-group">
                                        <label>Status Pernikahan</label>
                                            <input type="text" name="status_pernikahan" class="form-control" id="status_pernikahan" readonly >
						            </div>
                                    <div class="form-group">
                                        <label>Status Pernikahan</label>
                                            <input type="text" name="status_pernikahan" class="form-control" id="status_pernikahan" readonly >
						            </div>
                                    <div class="form-group">
                                        <label>Status Pernikahan</label>
                                            <input type="text" name="status_pernikahan" class="form-control" id="status_pernikahan" readonly >
						            </div>
                                        </ul>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
	</div>

	<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalEditJemaat" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cancel</span>
                                </button>
                                    <h4 class="modal-title" id="labelModalKu">Update Company</h4>
                        </div>
                            <div class="modal-body" id="body-replace">
                                <form id="form" enctype="multipart/form-data" action="<?php echo base_url($this->session->userdata('role').'/jemaat/updateJemaat'); ?>" method="post">
                                            <input type="text" name="id_jemaat"  id="id_jemaat" hidden>
								
						            <div class="form-group">
                                        <label>Alamat</label>
                                            <textarea type="text" name="alamat" class="form-control"  id="alamat" autocomplete="off" required="required"></textarea>
						            </div>
								
						            <div class="form-group">
                                        <label>No. Telepon</label>
                                            <input type="text" name="no_telepon" class="form-control" onkeypress = "return hanyaAngka(event)" id="no_telepon" autocomplete="off" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                            <input type="text" name="email" class="form-control" id="email" autocomplete="off" required="required">
									</div>
									
									<div class="form-group">
                            			<label for="komisi_jemaat">Komisi Jemaat</label>
                                			<select name="komisi_jemaat" id="komisi_jemaat" class="form-control">
                                        		<?php 
                                            		foreach($komisi_jemaat as $row => $value){
                                               			foreach($value as $row => $nilai){
                                        		?>
                                        		<option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        		<?php } }?>
                                			</select>
                                    </div>
                                    
									<div class="form-group">
                            			<label for="komisi_jemaat">Komisi Jemaat</label>
                                			<select name="komisi_jemaat" id="komisi_jemaat" class="form-control">
                                        		<?php 
                                            		foreach($komisi_jemaat as $row => $value){
                                               			foreach($value as $row => $nilai){
                                        		?>
                                        		<option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        		<?php } }?>
                                			</select>
                                    </div>
                                    
									<div class="form-group">
                            			<label for="komisi_jemaat">Komisi Jemaat</label>
                                			<select name="komisi_jemaat" id="komisi_jemaat" class="form-control">
                                        		<?php 
                                            		foreach($komisi_jemaat as $row => $value){
                                               			foreach($value as $row => $nilai){
                                        		?>
                                        		<option value="<?= $nilai ?>"><?= $nilai ?></option>
                                        		<?php } }?>
                                			</select>
                            		</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <button type="submit" name="button" class="btn btn-primary">Ubah</a></button>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
	</div>

	<div class="clearfix">
		<div class="pull-right tableTools-container"></div>
             <div class="modal fade" id="modalDeleteJemaat" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#3c8dbc; color:white;">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cancel</span>
                                </button>
                                    <h4 class="modal-title" id="labelModalKu">Delete Company</h4>
                        </div>
                            <div class="modal-body" id="body-delete">
                                <form id="form" enctype="multipart/form-data" action="<?php echo base_url($this->session->userdata('role').'/jemaat/deleteJemaat'); ?>" method="post">
                                        	<input type="text" name="id_jemaat"  id="id_jemaat" hidden>

                                    <div class="form-group">
                                        <label>Nama Jemaat</label>
                                            <input type="text" name="nama_jemaat" readonly="readonly" class="form-control" id="nama_jemaat" autocomplete="off" style="border-width:0px; font-size:20px; background-color: yellow" required="required">
									</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <button type="submit" name="button" class="btn btn-primary">Hapus</a></button>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
	</div>
	
<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
	function getStatuJemaat(){
		confirm("Apakah anda yakin untuk menyimpan ini? Hati-hati!! Data tidak dapat dihapus setelah tersimpan");
	}
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_lahir_edit').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_lahir_edit").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('tgl_lahir_edit', startDate);
                if ($("#tgl_lahir_edit").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
        function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
<script type="text/javascript"> 
	jQuery(function($) {
		var dateToday = new Date();
		$('#tgl_bergabung_edit').datepicker({
			dateFormat: 'yyyy-mm-dd',
			maxDate: dateToday,
			todayHighlight: true,
			autoclose: true,
		});

		$("#tgl_bergabung_edit").on('changeDate', function(selected) {
			var startDate = new Date(selected.date.valueOf());
			$("#end_period").datepicker({
				todayHighlight: true,
				autoclose: true,
				dateFormat: 'yyyy-mm-dd',
			});
			if ($("#tgl_bergabung_edit").val() > $("#end_period").val()) {
				;
			}
		});
	})

</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_keluar_edit').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_keluar_edit").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('tgl_keluar_edit', startDate);
                if ($("#tgl_keluar_edit").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>

<!-- <script type="text/javascript">
	$('#load1').on('click', function() {
    	var $this = $(this);
  	$this.button('loading');
    	setTimeout(function() {
       $this.button('reset');
	   }, 2000);
	});
	function showText()
{
    var itemlist = document.getElementsByClassName("test");
    function additem(index) {
        setTimeout(function(){itemlist[index].setAttribute("style", "display:block;", document.getElementById("load1").remove());}, 1000);
    }
    for(var i=0;i<itemlist.length;++i) {
        additem(i);
    } 
}
</script> -->

<script type="text/javascript">
	$(document).on("click", "#info_jemaat", function() {
		var id_jemaat = $(this).data("id_jemaat");
		var nama_jemaat = $(this).data("nama_jemaat");
		var tempat_lahir = $(this).data("tempat_lahir");
		var tgl_lahir = $(this).data("tgl_lahir");
		var jenis_kelamin = $(this).data("jenis_kelamin");
		var status_pernikahan = $(this).data("status_pernikahan");
		var tgl_pernikahan_info = $(this).data("tgl_pernikahan_info");
		var alamat = $(this).data("alamat");
		var no_telepon = $(this).data("no_telepon");
		var email = $(this).data("email");
		var tgl_babtis_info = $(this).data("tgl_babtis_info");
		var tgl_sidi_info = $(this).data("tgl_sidi_info");
		var status_jemaat_info = $(this).data("status_jemaat_info");
		var komisi_jemaat = $(this).data("komisi_jemaat");
		var tgl_bergabung = $(this).data("tgl_bergabung");
		var tgl_keluar = $(this).data("tgl_keluar");
		var note = $(this).data("note");


		$("#body-info #id_jemaat").val(id_jemaat);
		$("#body-info #nama_jemaat").val(nama_jemaat);
		$("#body-info #tempat_lahir").val(tempat_lahir);
		$("#body-info #tgl_lahir").val(tgl_lahir);
		$("#body-info #jenis_kelamin").val(jenis_kelamin);
		$("#body-info #status_pernikahan").val(status_pernikahan);
		$("#body-info #tgl_pernikahan_info").val(tgl_pernikahan_info);
		$("#body-info #alamat").val(alamat);
		$("#body-info #no_telepon").val(no_telepon);
		$("#body-info #email").val(email);
		$("#body-info #tgl_babtis_info").val(tgl_babtis_info);
		$("#body-info #tgl_sidi_info").val(tgl_sidi_info);
		$("#body-info #status_jemaat_info").val(status_jemaat_info);
		$("#body-info #komisi_jemaat").val(komisi_jemaat);
		$("#body-info #tgl_bergabung").val(tgl_bergabung);
		$("#body-info #tgl_keluar").val(tgl_keluar);
		$("#body-info #note").val(note);
	})  
</script>

<script type="text/javascript">
	$(document).on("click", "#edit_jemaat", function() {
		var id_jemaat = $(this).data("id_jemaat");
		var nama_jemaat = $(this).data("nama_jemaat");
		var tempat_lahir = $(this).data("tempat_lahir");
		var tgl_lahir_edit = $(this).data("tgl_lahir_edit");
		var jenis_kelamin = $(this).data("jenis_kelamin");
		var status_pernikahan = $(this).data("status_pernikahan");
		var tgl_pernikahan_edit = $(this).data("tgl_pernikahan_edit");
		var alamat = $(this).data("alamat");
		var no_telepon = $(this).data("no_telepon");
		var email = $(this).data("email");
		var tgl_babtis_edit = $(this).data("tgl_babtis_edit");
		var tgl_sidi_edit = $(this).data("tgl_sidi_edit");
		var status_jemaat_edit = $(this).data("status_jemaat_edit");
		var komisi_jemaat = $(this).data("komisi_jemaat");
		var tgl_bergabung_edit = $(this).data("tgl_bergabung_edit");
		var tgl_keluar_edit = $(this).data("tgl_keluar_edit");
		var note = $(this).data("note");


		$("#body-replace #id_jemaat").val(id_jemaat);
		$("#body-replace #nama_jemaat").val(nama_jemaat);
		$("#body-replace #tempat_lahir").val(tempat_lahir);
		$("#body-replace #tgl_lahir_edit").val(tgl_lahir_edit);
		$("#body-replace #jenis_kelamin").val(jenis_kelamin);
		$("#body-replace #status_pernikahan").val(status_pernikahan);
		$("#body-replace #tgl_pernikahan_edit").val(tgl_pernikahan_edit);
		$("#body-replace #alamat").val(alamat);
		$("#body-replace #no_telepon").val(no_telepon);
		$("#body-replace #email").val(email);
		$("#body-replace #tgl_babtis_edit").val(tgl_babtis_edit);
		$("#body-replace #tgl_sidi_edit").val(tgl_sidi_edit);
		$("#body-replace #status_jemaat_edit").val(status_jemaat_edit);
		$("#body-replace #komisi_jemaat").val(komisi_jemaat);
		$("#body-replace #tgl_bergabung_edit").val(tgl_bergabung_edit);
		$("#body-replace #tgl_keluar_edit").val(tgl_keluar_edit);
		$("#body-replace #note").val(note);
	})  
</script>


<script type="text/javascript">
	$(document).on("click", "#delete_jemaat", function() {
		var id_jemaat = $(this).data("id_jemaat");
		var nama_jemaat = $(this).data("nama_jemaat");

		$("#body-delete #id_jemaat").val(id_jemaat);
		$("#body-delete #nama_jemaat").val(nama_jemaat);
	})  
</script>

<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_pernikahan_edit').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_pernikahan_edit").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_pernikahan_edit").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_babtis_edit').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_babtis_edit").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_babtis_edit").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>
<script type="text/javascript"> 
	jQuery(function($) {
        var dateToday = new Date();
            $('#tgl_sidi_edit').datepicker({
                dateFormat: 'yyyy-mm-dd',
                maxDate: dateToday,
                todayHighlight: true,
                autoclose: true,
            });

            $("#tgl_sidi_edit").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#datepickerEnd").datepicker('setStartDate', startDate);
                if ($("#tgl_sidi_edit").val() > $("#datepickerEnd").val()) {
                    ;
                }
            });
        })
</script>