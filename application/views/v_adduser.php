<?php
	$role = $this->session->userdata('role');
?>
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-7">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Baru</h3>
                    </div>
                        <?= $this->session->flashdata('message'); ?>
                            <form class="form-horizontal" method="post" action="<?php echo base_url('/login/registration'); ?>">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="username" class="col-sm-4 control-label">Username</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="username" class="form-control" id="username" placeholder="Username" value="<?= set_value('username') ?>">
                                                <?= form_error('username', '<span class="text-danger">', '</span>'); ?>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword1" class="col-sm-4 control-label">Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="password1" id="inputPassword1" placeholder="Password">
                                                <?= form_error('password1', '<span class="text-danger">', '</span>'); ?>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-sm-4 control-label">Ulangi Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="password2" id="inputPassword2" placeholder="Ulangi Password">
                                            </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <a href="<?php echo base_url('/login'); ?>" class="btn btn-default pull-left">Kembali</a>
                                        <button type="submit" class="btn btn-info pull-right">Simpan</button>
                                 </div>
                            </form>
                        </div>
            </div>
    </div>
</section>
