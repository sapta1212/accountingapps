<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Accounting Apps</title>
  <link rel="icon" href="<?= BASE_ASSET; ?>dist/img/logo/sambros02.png" type="image/ico">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/googleapis.css">
</head>
<?php

// if ($this->session->userdata('username') != NULL) {
// 	redirect('home');
// }

?>



<body class="hold-transition login-page">
  <div style="background: url('assets/dist/img/login.svg') no-repeat; background-size: cover; height:100vh; " class="responsive">

    <div class="login-box" style="margin-top:70px;">
      <div class="login-box-body" style="border-radius:8px; box-shadow: 5px 10px 18px #888888;">

        <img style="margin-left:50px;" src='<?= BASE_ASSET; ?>dist/img/logo/sambros.svg' width="200" height="150" />

        <h3>
          <p class="login-box-msg">Register</p>
        </h3>
        <?= $this->session->flashdata('message'); ?>
        <form action="<?php echo base_url('login/registration'); ?>" method="post">

          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="Username" value="<?= set_value('username') ?>">
            <span span class="glyphicon glyphicon-user form-control-feedback"></span>
            <?= form_error('username', '<span class="text-danger">', '</span>');  ?>
          </div>

          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password1" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?= form_error('password1', '<span class="text-danger">', '</span>');  ?>
          </div>

          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password2" placeholder="Confirm Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?= form_error('password2', '<span class="text-danger">', '</span>');  ?>
          </div>


          <div class="row">
            <!-- /.col -->
            <br>
            <div class="col-xs-12 text-center">
              <button type="submit" class="btn btn-warning btn-block btn">Register</button>
              <br>
              <a href="<?php echo base_url('/login'); ?>">Back</a>
            </div>
            <br>

            <!-- /.col -->
          </div>
        </form>

        <!-- /.social-auth-links -->

      </div>
      <!-- /.login-box-body -->
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery 3 -->
  <script href="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script href="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
  <script>
    // $(function () {
    //   $('input').iCheck({
    //     checkboxClass: 'icheckbox_square-blue',
    //     radioClass: 'iradio_square-blue',
    //     increaseArea: '20%' /* optional */
    //   });
    // });
  </script>
</body>

</html>