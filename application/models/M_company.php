<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_company extends CI_Model
{

    public function getCountCompany()
    {
        $this->db->from('company');
        $this->db->order_by('name', 'ASC');
        return $this->db->get();
    }

	public function pic()
	{
		$this->db->select('name');
		return $this->db->get('pic');
    }
    
	public function getCompany($perPage, $start)
	{
		$this->db->order_by('id_company','ASC');
		$this->db->limit($perPage, $start);
		return $this->db->get('company');
	}

	public function createCompany($data)
	{
		$this->db->insert('company', $data);
	}

}
