<?php

class M_pic extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCountPic()
    {
        $this->db->from('pic');
        $this->db->order_by('name', 'ASC');
        return $this->db->get();
    }

	public function getPic($perPage, $start)
	{
		$this->db->order_by('id_pic','ASC');
		$this->db->limit($perPage, $start);
		return $this->db->get('pic');
    }
    
	public function createPic($data)
	{
		$this->db->insert('pic', $data);
    }
    
    public function updatePic($data)
    {
        $idPic = (int)$data['id_pic'];
        $this->db->where('id_pic',$idPic);
        $this->db->update('pic', $data);
    }

    public function deletePic($idPic)
    {
        $this->db->where('id_pic', $idPic);
        $this->db->delete('pic');
    }

    public function getSearchCountPic($keyword)
    {
		$this->db->where('p.name', $keyword);
		$this->db->from('pic p');
		$this->db->select('p.id_pic, p.name, p.email, p.phone, p.address');
		return $this->db->get();
    }
    
    public function getSearchPic($keyword,$perPage, $start)
    {
		$this->db->where('p.name', $keyword);
		$this->db->from('pic p');
		$this->db->select('p.id_pic, p.name, p.email, p.phone, p.address');
        $this->db->limit($perPage, $start);
		return $this->db->get();
    }
    
}
