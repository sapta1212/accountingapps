<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

	public function Login($u, $p)
	{
		$user = $this->db->get_where('users', ['username' => $u])->row_array();
		$password = md5($p);
		$user2 = $user['id'].$user['username'].$user['password'];
		$iduser = $user['id'];
		$user3 = $iduser.$u.$password;

		if ($user['status'] == 'true') {
			if($user2 == $user3) {
				$data = [
					'id'		=> $user['id'],
					'username'	=> $user['username'],
					'password'	=> $password,
					'role'		=> $user['role'],
					'status'	=> $user['status']
				];
				$this->session->set_userdata($data);
					if ($user['role'] == 'admin') {
						redirect('admin/home');
					} else if($user['role'] == 'user'){
						redirect('user/home');
					}else{
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username atau Password Salah!!</div>');
						redirect('login');
					}
					
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username atau Password Salah!</div>');
				redirect('login');
			}
				
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Akun tidak terdaftar atau tidak aktif, silakan hubungi admin!</div>');
			redirect('login');
		}
		
	}

	public function getLogin($u, $p)
	{
		$pwd = md5($p);
		$this->db->where('username', $u);
		$this->db->where('password', $pwd);

    	$query = $this->db->get('users');
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$sess = array(		'id'   				=> $row->iduser,
									'username'     => $row->username,
									'password'     => $row->password,
									'deleted'      => $row->deleted,
									'level'			=> $row->level);

				$this->session->set_userdata($sess);
				$this->load->model('m_user');
				$this->m_user->getlastlogin($u);
			}

			if ($this->session->userdata('deleted') == '1') {
				$this->session->set_flashdata('info', '<p style="color:red;">User Anda sudah tidak aktif!!!</p>');
				redirect('login');

			}else if ($this->session->userdata('level') == 'admin') {
				header('location:'.base_url().'user/home');

			}else if ($this->session->userdata('level') == 'user') {
				header('location:'.base_url().'user/home');
			}

		}else {
			$this->session->set_flashdata('info', 'Username or Password Wrong!');
			redirect('login');
    	}
	}

	public function addUser($data)
	{
		$this->db->insert('users', $data);
	}


}
