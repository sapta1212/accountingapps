<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function addUser($data)
	{
		var_dump($data);die;
		$this->db->insert('users', $data);
	}

	public function getUsers($perPage, $start)
	{
		$this->db->order_by('username','ASC');
		$this->db->limit($perPage, $start);
		return $this->db->get('users');
	}

	public function getCount()
	{
		$this->db->from('users');
		$this->db->order_by('username','ASC');
		return $this->db->get();
	}

	public function updatePass($iduser, $newpass)
	{
		$this->db->set('password', $newpass);
		$this->db->where('id', $iduser);
		$this->db->update('users');
	}

	public function userAccess()
	{
		$this->db->select('level');
		return $this->db->get('level');
	}

	public function editUser($data)
	{
		$username = $data['username'];
		$status = $data['status'];
		if($status == 'Aktif'){
			$status = 'true';
		}else{
			$status = 'false';
		}
		$this->db->set('status', $status);
		$this->db->where('username', $username);
		$this->db->update('users');
	}
	
	
}
