<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pic extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pic');
    }


    public function dataPic($id = null)
    {
        $isi['content'] = 'v_datapic';
        $jml = $this->M_pic->getCountPic()->num_rows();

        $config['base_url'] = base_url() . 'company/dataPic';
        $config['total_rows'] = $jml;
        $config['per_page'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $isi['halaman'] = $this->pagination->create_links();
        $data_pic = $this->M_pic->getPic($config['per_page'], $id)->result();
        $stdcode = json_decode(json_encode($data_pic), true);
        $isi['data_pic'] = $stdcode;
        $this->load->view('v_home', $isi);
    }

    public function addPic()
    {
        $isi['content'] = 'v_addpic';

        $this->load->view('v_home', $isi);
    }

    public function savePic()
    {
        $this->form_validation->set_rules('name', 'Company Name', 'required', [
            'required' => 'Must fill Company Name!'
        ]);
        $this->form_validation->set_rules('address', 'Address', 'required', [
            'required' => 'Must fill Address!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required', [
            'required' => 'Must fill Email!'
        ]);
        $this->form_validation->set_rules('phone', 'TPhone Number', 'required', [
            'required' => 'Must fill Phone Number!'
        ]);

        if ($this->form_validation->run() == false) {
            $isi['content'] = 'v_addpic';

            $this->load->view('v_home', $isi);
        } else {

            $id = $this->db->query('SELECT MAX(id_pic) AS maxid FROM pic')->row();
            $stdcode = json_decode(json_encode($id), true);
            $id_pic = (int) $stdcode['maxid'];

            $data = [
                'id_pic'         => $id_pic + 1,
                'name'               => htmlspecialchars($this->input->post('name')),
                'email' => htmlspecialchars($this->input->post('email')),
                'phone'         => htmlspecialchars($this->input->post('phone')),
                'address'     => htmlspecialchars($this->input->post('address'))
            ];
            // var_dump($data);die;
            $this->M_pic->createPic($data);
            $this->session->set_flashdata('success', 'PIC data was saved successfully');
            redirect('pic/dataPic');
        }
    }

    public function editPic()
    {
        $data = [
            'id_pic'    => $this->input->post('id_pic_edit'),
            'name'      => htmlspecialchars($this->input->post('name_edit')),
            'email'     => htmlspecialchars($this->input->post('email_edit')),
            'phone'     => htmlspecialchars($this->input->post('phone_edit')),
            'address'   => htmlspecialchars($this->input->post('address_edit'))
        ];

        $this->M_pic->updatePic($data);
        $this->session->set_flashdata('success', 'Data successfully changed');
        redirect('pic/dataPic');
    }

    public function deletePic()
    {
        $id_pic = [
            'id_pic'    => $this->input->post('id_pic_delete')
        ];
        $idPic = (int) $id_pic['id_pic'];

        // var_dump($idPic);die;

        $this->M_pic->deletePic($idPic);
        $this->session->set_flashdata('success', 'Data successfully deleted');
        redirect('pic/dataPic');
    }

    public function searchPic($id = NULL)
    {
        $keyword =  htmlspecialchars($this->input->post('keyword'));
        $isi['content'] = 'v_datapic';
        $isi['menu'] = "PIC Search Result";

        $data['searchby'] = $this->input->post('searchby');
        $data['keyword'] = $this->input->post('keyword');
        $data['search_name_pic'] = htmlspecialchars($this->input->post('nam_pem'));


        if ($data['searchby'] != "") {
            $category = $this->input->post('searchby');
            $this->session->set_userdata(array("category" => $category));
        } else {
            $category = $this->session->userdata('category');
        }

        if ($data['keyword'] != "") {
            $searchby = $this->input->post('searchby');
            $keyword = $this->input->post('keyword');
            $varKey = array('searchby' => $searchby, 'keyword' => $keyword);
            $this->session->set_userdata($varKey);
        } else {
            $searchby = $this->session->userdata('searchby');
            $keyword = $this->session->userdata('keyword');
        }
        if ($category == 'search_name_pic') {
            $jml = $this->M_pic->getSearchCountPic($keyword)->num_rows();

            $config['base_url'] = base_url() . 'pic/searchPic';
            $config['total_rows'] = $jml;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_page'] = '&laquo;';
            $config['prev_page'] = '&raquo;';
            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close'] = '</span></li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close'] = '</span>Next</li>';
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close'] = '</span></li>';
            $this->pagination->initialize($config);
            $isi['halaman'] = $this->pagination->create_links();

            $data_pic = $this->M_pic->getSearchPic($keyword, $config['per_page'], $id)->result();
            $stdcode = json_decode(json_encode($data_pic), true);
            $isi['data_pic'] = $stdcode;
        } else {

            $jml = $this->M_pic->getCountPic()->num_rows();

            $config['base_url'] = base_url() . 'pic/searchPic';
            $config['total_rows'] = $jml;
            $config['per_page'] = 9;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_page'] = '&laquo;';
            $config['prev_page'] = '&raquo;';
            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close'] = '</span></li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close'] = '</span>Next</li>';
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close'] = '</span></li>';

            $this->pagination->initialize($config);
            $isi['halaman'] = $this->pagination->create_links();
            $data_pic = $this->M_pic->getPic($config['per_page'], $id)->result();
            $stdcode = json_decode(json_encode($data_pic), true);
            $isi['data_pic'] = $stdcode;
        }
        $this->load->view('v_home', $isi);
    }
}
