<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        logged_in();
        access_admin();
        $this->load->model('M_user');
    }

    public function index($id = null) {
        $isi['content'] = 'admin/v_datausers';
        $jml = $this->M_user->getCount()->num_rows();

        $config['base_url'] = base_url() . 'admin/user/index';
        $config['total_rows'] = $jml;
        $config['per_page'] = 10;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $isi['halaman'] = $this->pagination->create_links();
        $isi['userdata'] = $this->M_user->getUsers($config['per_page'], $id)->result();
        $this->load->view('admin/v_home', $isi);
    }

    public function editPass()
    {
        $isi['content'] = 'admin/v_changepass';
        $this->load->view('admin/v_home', $isi);
    }

    public function edit()
    {
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);

        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        $sespass = $this->db->get_where('users', ['username' => 
        $this->session->userdata('username')])->row_array();

        if ($this->form_validation->run() == false) {
            $isi['content'] = 'admin/v_changepass';
            $this->load->view('admin/v_home', $isi);
        }else{

            $u       = $this->session->userdata('username');
            $p       = htmlspecialchars($this->input->post('password1'));
            $oldpass = htmlspecialchars($this->input->post('oldpass'));
            $op      = md5($oldpass);
            $iduser  = $sespass['id'];

            $pass    = $iduser.$u.$op;
            if ($pass == $iduser.$sespass['username'].$sespass['password']) {
                $newpass = md5($p);
    
                $this->M_user->updatePass($iduser, $newpass);
    
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Change Password Successfully!</div>');
                redirect('admin/user/editPass');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Current Password Wrong!</div>');
                redirect('admin/user/editPass');
            }
            
        }
    }

    public function add() {
        $isi['content'] = 'admin/v_adduser';

        // multidimension array to single array
        $access = $this->M_user->userAccess()->result();
        $roleaccess = json_decode(json_encode($access), true);
        $isi['role_access'] = $roleaccess;
        
        $this->load->view('admin/v_home', $isi);
    }

    public function registration() {

        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]', [
            'is_unique' => 'Username sudah ada!!!'
        ]);
        $this->form_validation->set_rules('level', 'Level', 'required', [
            'required' => 'Level harus diisi!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password terlalu pendek!'
        ]);

        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        $access = $this->M_user->userAccess()->result();
        $roleaccess = json_decode(json_encode($access), true);
        $isi['role_access'] = $roleaccess;

        if ($this->form_validation->run() == false) {
            $isi['content'] = 'admin/v_adduser';
            $this->load->view('admin/v_home', $isi);

        } else {
            $iduser = $this->db->query('SELECT MAX(id) AS maxid FROM users')->row();
            $idarray = json_decode(json_encode($iduser), true);
            $idconvert = $idarray['maxid'];
        
            $data = [
                'id' => $idconvert + 1,
                'username' => htmlspecialchars($this->input->post('username')),
                'password' => htmlspecialchars(md5($this->input->post('password1'))),
                'role' => htmlspecialchars($this->input->post('level')),
                'status' => 'false'
            ];

            $this->M_user->addUser($data);
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect('admin/user');
        }
    }

    public function updateStatus()
    {
        $data = [
            'username'  => $this->input->post('username'),
            'status'    => htmlspecialchars($this->input->post('status'))
        ];
        $this->M_user->editUser($data);
        $this->session->set_flashdata('success', 'Data berhasil dirubah');
        redirect('admin/user');

    }

}
