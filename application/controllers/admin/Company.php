<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Company extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_company');
    }

    public function dataCompany($id = null)
    {
        $isi['content'] = 'v_datacompany';
        $jml = $this->M_company->getCountCompany()->num_rows();

        $pic = $this->M_company->pic()->result();
        $pic_com = json_decode(json_encode($pic), true);
        $isi['pic_name'] = $pic_com;

        $config['base_url'] = base_url() . 'company/dataCompany';
        $config['total_rows'] = $jml;
        $config['per_page'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $isi['halaman'] = $this->pagination->create_links();
        $data_company = $this->M_company->getCompany($config['per_page'], $id)->result();
        $stdcode = json_decode(json_encode($data_company), true);
        $isi['data_company'] = $stdcode;
        $this->load->view('v_home', $isi);
    }

    public function addCompany() {
        $isi['content'] = 'v_addcompany';

        $pic = $this->M_company->pic()->result();
        $pic_com = json_decode(json_encode($pic), true);
        $isi['pic_name'] = $pic_com;

        $this->load->view('v_home', $isi);
    }

    public function saveCompany()
    {
        $this->form_validation->set_rules('name', 'Company Name', 'required', [
            'required' => 'Must fill Company Name!'
        ]);
        $this->form_validation->set_rules('phone_number', 'TPhone Number', 'required', [
            'required' => 'Must fill Phone Number!'
        ]);
        $this->form_validation->set_rules('address', 'Address', 'required', [
            'required' => 'Must fill Address!'
        ]);
        $this->form_validation->set_rules('pic_01', 'PIC 1', 'required', [
            'required' => 'Must choose PIC 1!'
        ]);

        if($this->form_validation->run() == false){
            $isi['content'] = 'v_addcompany';

            $pic = $this->M_company->pic()->result();
            $pic_com = json_decode(json_encode($pic), true);
            $isi['pic_name'] = $pic_com;

            $this->load->view('v_home', $isi);
        }else{

            $id_company = $this->db->query('SELECT MAX(id_company) AS maxid FROM company')->row();
            $stdcode = json_decode(json_encode($id_company), true);
            $id_com = (int)$stdcode['maxid'];

            $name_com = htmlspecialchars($this->input->post('name'));
            $data = [
                'id_company'         => $id_com + 1,
                'name'               => $name_com,
                'phone_number'       => htmlspecialchars($this->input->post('phone_number')),
                'address'            => htmlspecialchars($this->input->post('address')),
                'pic1'               => htmlspecialchars($this->input->post('pic_01')),
                'pic2'               => htmlspecialchars($this->input->post('pic_02')),
                'pic3'               => htmlspecialchars($this->input->post('pic_03'))
            ];
            if($data['pic2'] == null){
                $data['pic2'] == '-';
            }
            if($data['pic3'] == null){
                $data['pic3'] == '-';
            }
            // var_dump($data);die;

			$config['upload_path'] = './assets/upload/company/';
			$config['allowed_types'] = 'png';
			$config['file_name'] = $name_com;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(!$this->upload->do_upload('img')){
                // print_r($this->upload->display_errors());die;
                $this->session->set_flashdata('failed', 'Company data was not saved successfully');
                redirect('company/dataCompany');
            }else{
                $this->M_company->createCompany($data);
                $this->session->set_flashdata('success', 'Company data was saved successfully');
                redirect('company/dataCompany');
            }
        }
        
    }
}
