<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_login');

	}

	public function index()
	{
		$this->load->view('v_login');
	}

	public function getLogin()
	{

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('v_login');
		}else {
			$u = htmlspecialchars($this->input->post('username'));
			$p = htmlspecialchars($this->input->post('password'));
			$this->load->model('M_login');
			$this->M_login->Login($u, $p);
		}

	}

	public function addUser(){
        $this->load->view('v_register');
	}

	public function registration(){

        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]', [
            'is_unique' => 'Username sudah ada!!!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password terlalu pendek!'
        ]);

        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
		
		if ($this->form_validation->run() == false){
			$isi['content'] = 'v_register';
			$this->load->view('v_register');
		} else {
			$idUser = $this->db->query('SELECT MAX(id_users) AS maxid FROM users')->row();
			$id_array = json_decode(json_encode($idUser), true);
			$id_convert = $id_array['maxid'];

			$pass = htmlspecialchars($this->input->post('password1'));
			$salt = substr(sha1(uniqid(rand(), true)), 0, 10);
			$password = sha1(sha1($salt.$pass.$salt));

			$data = [
			'id_users'		=> $id_convert + 1,
			'username' 	=> htmlspecialchars($this->input->post('username')),
			'password' 	=> $password,
			'salt'		=> $salt,
			'role'		=> 'users',
			'status'	=> 'true'
			];
			// var_dump($data);die;

			$this->M_login->addUser($data);
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect('/login');


		}

	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		$this->session->sess_destroy();
		redirect('login');
	}
}
