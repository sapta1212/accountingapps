const flashsuccess = $('.success-sapta').data('flashdata');
const flashfailed = $('.failed-sapta').data('flashdata');
const flashinfo = $('.info-sapta').data('flashdata');

if (flashsuccess) {
	Swal.fire({
		position: 'center',
		type: 'success',
		title: flashsuccess,
		showConfirmButton: false,
		timer: 2000
	});
}

if (flashfailed) {
	Swal.fire({
		position: 'center',
		type: 'error',
		title: flashfailed,
		showConfirmButton: false,
		timer: 8000
	});
}


if (flashinfo) {
	Swal.fire({
		position: 'center',
		type: 'info',
		title: flashinfo,
		showConfirmButton: false,
		timer: 8000
	});
}



