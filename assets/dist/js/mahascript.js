const flashdata = $('.flash-data').data('flashdata');

if (flashdata) {
   Swal.fire({
      position: 'center',
      type: 'success',
      title: flashdata,
      showConfirmButton: false,
      timer: 2000
   });
}




$('.tombol-inactive').on('click', function (e) {

   e.preventDefault();

   const href = $(this).attr('href');

   Swal.fire({
      title: 'Are you sure?',
      text: "Are you sure to deactivated this user!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, deactivated!'
   }).then((result) => {
      if (result.value == true) {
         document.location.href = href;
      }
   })

});

$('.tombol-active').on('click', function (e) {

   e.preventDefault();

   const href = $(this).attr('href');

   Swal.fire({
      title: 'Are you sure?',
      text: "Are you sure to activate this user!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activated!'
   }).then((result) => {
      if (result.value) {
         document.location.href = href;
      }
   })

});