const flash = $('.info-data').data('flashdata');

if (flash) {
   Swal.fire({
      type: 'warning',
      title: 'API KEY : ' + flash,
      text: 'Please note, because Key Will not Saved',
      footer: 'Please keep the confidentiality for secure',
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false
   });
}

const kilat = $('.info').data('flashdata');
if (kilat) {
   Swal.fire({
      position: 'center',
      type: 'success',
      title: kilat,
      showConfirmButton: false,
      timer: 1500
   });
}